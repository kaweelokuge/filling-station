/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Kavii
 */
public class PetroliumClass extends ProductClass{
    private double CurrentQuantity;
    
    //Result Set
    ResultSet rs=null;
    
     public void SetPetrolium(String pProductName,double pSalesPrice,double pCurrentQuantity){
         
       super.ProductName=pProductName;
       super.SalesPrice=pSalesPrice;
       this.CurrentQuantity=pCurrentQuantity;
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="INSERT INTO petrolium(Name,SalesPrice,Current_Quantity) VALUES('"+ProductName+"','"+SalesPrice+"','"+CurrentQuantity+"')";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
   //Get Petrolium Names
   
     public ResultSet GetPetroliumName(){
         
       con=DBAccess.DBconnect.connect();
      
         try {
            String sql="SELECT Name FROM petrolium";
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        return rs;
        
    }
   
     
    
   //Update Petrolium
    
    public void UpdatePetrolium(String pProductID,String pProductName,double pSalesPrice){
       super.ProductID=pProductID;  
       super.ProductName=pProductName;
       super.SalesPrice=pSalesPrice;
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="UPDATE petrolium SET Name = '"+ProductName+"',SalesPrice='"+SalesPrice+"' WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    //Get PetroliumID
    
    public String GetPetroliumID(String pProductName){
        this.ProductName=pProductName;
        
        con=DBAccess.DBconnect.connect();
      
        try {
            String q="SELECT ProductID FROM petrolium WHERE Name='"+ProductName+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.ProductID=rs.getString(1);
            }
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.ProductID;
        
    }
    
    //Update Petrolium Quantity
    
    public void UpdatePetroliumQuantity(int pProductID,double pNewQuantity){
       super.ProductID=Integer.toString(pProductID);  
       double NewQuantity=pNewQuantity;
      
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="SELECT Current_Quantity FROM petrolium WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.CurrentQuantity=rs.getDouble(1);
            }
            //Code Here
            this.CurrentQuantity=this.CurrentQuantity+NewQuantity;
            
            
            q="UPDATE petrolium SET Current_Quantity = '"+CurrentQuantity+"' WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    //Delete Petrolium
    public void DeletePetrolium(String pPetroliumID){
        this.ProductID=pPetroliumID;
        con=DBAccess.DBconnect.connect();
        
        
        try {
            String q="DELETE FROM petrolium WHERE ProductID='"+ProductID+"'";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
    
    }
    
    


}
