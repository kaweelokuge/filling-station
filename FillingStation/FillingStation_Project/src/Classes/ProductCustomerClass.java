/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;


import java.awt.Component;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Kavii
 */
public class ProductCustomerClass {
    private int PaymentID;
//    private int EmployeeID; //Optional
    private String ProductType;
    private int ProductID;
    private Date SoldDate;
    private String PaymentMethod;
    private static double TotalAmount;
    
    //Petrloium Details
    private double PetroliumSalesPrice;
    private double SelectedPetroliumQuantity_Litre;
    private double PetroliumCurrentQuantity;//Petrolium Quantity in Litres Before Update
    
    //MiscellaneousItem Details
    private double MiscellaneousItemUnitPrice;
    private int MiscellaneousItemCurrentQuantity;//MiscellaneousItem Units Before Update
    private double MiscellaneousItemAmount_Currency;
     
    public Connection con=null;
    public PreparedStatement pst = null;
    //Result Set
    ResultSet rs=null;
    
    public int setPetroliumCustomer(String pSelectedPetrolimItem,double pSelectedPetroliumAmount_Currency){
       
       
       
       con=DBAccess.DBconnect.connect();
       //Get Petrolium ID
       try {
            String q="SELECT ProductID FROM petrolium WHERE Name='"+pSelectedPetrolimItem+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.ProductID=rs.getInt(1);
            }
           
        } catch (SQLException ex) {
            System.out.println(ex);
        }
       
       //Get Petrolium Sales Price
        try {
            String q="SELECT SalesPrice FROM petrolium WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.PetroliumSalesPrice=rs.getDouble(1);
            }
            this.SelectedPetroliumQuantity_Litre=pSelectedPetroliumAmount_Currency/this.PetroliumSalesPrice;
           
        } catch (SQLException ex) {
            System.out.println(ex);
        }
      
       //Update Petrolium Quantity
         try {
            String q="SELECT Current_Quantity FROM petrolium WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.PetroliumCurrentQuantity=rs.getDouble(1);
            }
            
            if(this.PetroliumCurrentQuantity<SelectedPetroliumQuantity_Litre){
                    Component frame=null;
                    JOptionPane.showMessageDialog(frame,"Cannot Update! Petrolium Stock Out of Order!","Warning",JOptionPane.WARNING_MESSAGE);
                    return -1;
            }
            
            this.PetroliumCurrentQuantity=this.PetroliumCurrentQuantity-this.SelectedPetroliumQuantity_Litre;
            
            
            q="UPDATE petrolium SET Current_Quantity = '"+this.PetroliumCurrentQuantity+"' WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
       
       ProductCustomerClass.TotalAmount=ProductCustomerClass.TotalAmount+pSelectedPetroliumAmount_Currency;
       return 0;
}
    
    public int setMiscellaneousItemCustomer(String pSelectedMiscellaneousItem,int pSelectedMiscellaneousItemQuantity){
    
        con=DBAccess.DBconnect.connect();
        //Get MiscellaneousItem ID
        try {
            String q="SELECT ProductID FROM miscellaneousitem WHERE Name='"+pSelectedMiscellaneousItem+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.ProductID=rs.getInt(1);
            }
            
        } catch (SQLException e) {
            System.out.println(e);
        }
     
        //Get MiscellaneousItem Unit Price
        try {
            String q="SELECT Unit_Price FROM miscellaneousitem WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.MiscellaneousItemUnitPrice=rs.getDouble(1);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        this.MiscellaneousItemAmount_Currency=this.MiscellaneousItemUnitPrice*pSelectedMiscellaneousItemQuantity;
        
    
        //Update MiscellaneousItem 
    
        try {
            String q="SELECT Current_No_Items FROM miscellaneousitem WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            rs=pst.executeQuery();
            
            if(rs.next()){
                this.MiscellaneousItemCurrentQuantity=rs.getInt(1);
            }
            
            if(this.MiscellaneousItemCurrentQuantity<pSelectedMiscellaneousItemQuantity){
                    Component frame=null;
                    JOptionPane.showMessageDialog(frame,"Cannot Update! MiscellaneousItem Stock Out of Order!","Warning",JOptionPane.WARNING_MESSAGE);
                    return -1;
            }
            
            this.MiscellaneousItemCurrentQuantity=this.MiscellaneousItemCurrentQuantity-pSelectedMiscellaneousItemQuantity;
            
            
            q="UPDATE miscellaneousitem SET Current_No_Items = '"+this.MiscellaneousItemCurrentQuantity+"' WHERE ProductID='"+ProductID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
        ProductCustomerClass.TotalAmount=ProductCustomerClass.TotalAmount+this.MiscellaneousItemAmount_Currency;
        return 0;
    }
    
    //Get Total Bill
    public double TotalBillAmount(){
        return ProductCustomerClass.TotalAmount;
    }
    //Get Petrolium Quantity
    public double OrderedPetroliumQuantity(){
        return this.SelectedPetroliumQuantity_Litre;//Ordered Petrolium Quantity on Litres
    }
    //Get MiscellaneousItem Amount
    public double MiscellaneousItemBilledAmount(){
        return this.MiscellaneousItemAmount_Currency;
    }
    
    //Add Final Payment into Sell Table
    public void SetSell(String pPaymentMethod){
        
        con=DBAccess.DBconnect.connect();
        
        DateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String PurchaseDate = dateFormat.format(calendar.getTime());
        System.out.println(PurchaseDate+pPaymentMethod+ProductCustomerClass.TotalAmount);
        
    
          try {
            String q="INSERT INTO sell(Sold_Date,Method,Total_Amount) VALUES('"+PurchaseDate+"','"+pPaymentMethod+"','"+ProductCustomerClass.TotalAmount+"')";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
        
    
    }
    
    
}
