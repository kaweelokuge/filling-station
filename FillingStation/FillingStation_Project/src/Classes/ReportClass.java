/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;



import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;

import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JOptionPane;




/**
 *
 * @author Kavii
 */
public class ReportClass {
    public Connection con=null;
    public PreparedStatement pst = null;
    public String Value;
    //Result Set
    ResultSet rs=null;
    
    
    
    
    public void GenerateproductpurchaseReport(){
        con=DBAccess.DBconnect.connect();
        
        try {
            String sql="SELECT * FROM productpurchase";
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            
        } catch (SQLException e) {
            System.err.println(e);
        }
        
        
        try {
            Document doc= new Document();
            PdfWriter.getInstance(doc,new FileOutputStream("Report1.pdf"));
            doc.open();
            doc.add(new Paragraph("Product Purchase",FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.BOLD,BaseColor.BLACK)));
            doc.add(new Paragraph(new Date().toString()));
            doc.add(new Paragraph("-----------------------------------------------------------------------------------"));
            PdfPTable table = new PdfPTable(5);
            
            
            
            table.addCell("BatchID");
            table.addCell("StockPrice");
            table.addCell("Quantity");
            table.addCell("TotalPayment");
            table.addCell("PurchaseDate");
            
            
            try {
                while(rs.next()){
                for(int i=1;i<=5;i++){
                    Value=rs.getString(i);
                    table.addCell(Value);
                }
                
            }
            } catch (SQLException e) {
                System.out.println(e);
            }
            
            doc.add(table);
            
            doc.close();
            JOptionPane.showMessageDialog(null,"Report ProductPurchase Saved");
            
            
            try {
                Desktop.getDesktop().open(new File("H:\\IMPORTANT DOCUMENTS\\KAWEE\\MIT\\Projects\\FillingStation_Project\\Report1.pdf"));
            } catch (IOException e) {
                System.out.println(e);
            }
            
        } catch (DocumentException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(null,e);
        }
        
        
        
    }
    
    public void GeneratesupplyReport(){
        con=DBAccess.DBconnect.connect();
        
        try {
            String sql="SELECT * FROM supply";
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            
        } catch (SQLException e) {
            System.err.println(e);
        }
        
        
        try {
            Document doc= new Document();
            PdfWriter.getInstance(doc,new FileOutputStream("Report2.pdf"));
            doc.open();
            doc.add(new Paragraph("Supply Details",FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.BOLD,BaseColor.BLACK)));
            doc.add(new Paragraph(new Date().toString()));
            doc.add(new Paragraph("-----------------------------------------------------------------------------------"));
            PdfPTable table = new PdfPTable(3);
            
            
            
            table.addCell("BatchID");
            table.addCell("SupplierID");
            table.addCell("ProductID");
            
            
            
            try {
                while(rs.next()){
                for(int i=1;i<=3;i++){
                    Value=rs.getString(i);
                    table.addCell(Value);
                }
                
            }
            } catch (SQLException e) {
                System.out.println(e);
            }
            
            doc.add(table);
            
            doc.close();
            JOptionPane.showMessageDialog(null,"Report Supply Details Saved");
            
            
            try {
                Desktop.getDesktop().open(new File("H:\\IMPORTANT DOCUMENTS\\KAWEE\\MIT\\Projects\\FillingStation_Project\\Report2.pdf"));
            } catch (IOException e) {
                System.out.println(e);
            }
            
        } catch (DocumentException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(null,e);
        }
        
        
        
    }
    
    
    public void GeneratesellReport(){
        con=DBAccess.DBconnect.connect();
        
        try {
            String sql="SELECT * FROM sell";
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            
        } catch (SQLException e) {
            System.err.println(e);
        }
        
        
        try {
            Document doc= new Document();
            PdfWriter.getInstance(doc,new FileOutputStream("Report3.pdf"));
            doc.open();
            doc.add(new Paragraph("Sales Details",FontFactory.getFont(FontFactory.TIMES_BOLD,18,Font.BOLD,BaseColor.BLACK)));
            doc.add(new Paragraph(new Date().toString()));
            doc.add(new Paragraph("-----------------------------------------------------------------------------------"));
            PdfPTable table = new PdfPTable(4);
            
            
            
            table.addCell("PaymentID");
            table.addCell("Sold_Date");
            table.addCell("Method");
            table.addCell("Total_Amount");
            
            
            
            try {
                while(rs.next()){
                for(int i=1;i<=4;i++){
                    Value=rs.getString(i);
                    table.addCell(Value);
                }
                
            }
            } catch (SQLException e) {
                System.out.println(e);
            }
            
            doc.add(table);
            
            doc.close();
            JOptionPane.showMessageDialog(null,"Report Sales Details Saved");
            
            
            try {
                Desktop.getDesktop().open(new File("H:\\IMPORTANT DOCUMENTS\\KAWEE\\MIT\\Projects\\FillingStation_Project\\Report3.pdf"));
            } catch (IOException e) {
                System.out.println(e);
            }
            
        } catch (DocumentException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(null,e);
        }
        
        
        
    }
    
    //
    
}
