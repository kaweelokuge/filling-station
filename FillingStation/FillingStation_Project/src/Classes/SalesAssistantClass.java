/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.sql.SQLException;

/**
 *
 * @author Kavii
 */
public class SalesAssistantClass extends EmployeeClass {
    
      public void SetSalesAssistant(String pNIC,String pF_Name,String pL_Name,String pTelephone,String pAddress){
         
       super.NIC=pNIC;
       super.F_Name=pF_Name;
       super.L_Name=pL_Name;
       super.Telephone=pTelephone;
       super.Address=pAddress;
       
       con=DBAccess.DBconnect.connect();
       
        try {
            String q="INSERT INTO salesassistant(NIC,F_Name,L_Name,Telephone,Address) VALUES('"+NIC+"','"+F_Name+"','"+L_Name+"','"+Telephone+"','"+Address+"')";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    
    //Update SalesAssistant
    
    public void UpdateSalesAssistant(String pEmployeeID,String pNIC,String pF_Name,String pL_Name,String pTelephone,String pAddress){
       this.EmpID=pEmployeeID;
       this.NIC=pNIC;
       this.F_Name=pF_Name;
       this.L_Name=pL_Name;
       this.Telephone=pTelephone;
       this.Address=pAddress;
       
       
       con=DBAccess.DBconnect.connect();
      
        try {
            String q="UPDATE salesassistant SET NIC = '"+NIC+"',F_Name='"+F_Name+"',L_Name='"+L_Name+"',Telephone='"+Telephone+"',Address='"+Address+"' WHERE EmployeeID='"+EmpID+"'  ";
            pst=con.prepareStatement(q);
            pst.execute();
        } catch (SQLException e) {
            System.out.println(e);
        }
       
    }
    
    //Delete SalesAssistant 
    public void DeleteSalesAssistant(String pEmployeeID){
        this.EmpID=pEmployeeID;
        con=DBAccess.DBconnect.connect();
        
        
        try {
            String q="DELETE FROM salesassistant WHERE EmployeeID='"+EmpID+"'";
            pst=con.prepareStatement(q);
            pst.execute();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
    
    }
    
      
      
      
}
