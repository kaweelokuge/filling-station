/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;



/**
 *
 * @author Kavii
 */
public class ValidateClass {
    public static boolean IsValid=true;
    
    
    
    /**Empty Validation**/
    //Registration and Employee Validate
    public static boolean CheckEmpty(String pNIC,String pF_Name,String pL_Name,String pTelephone,String pAddress,String pRole){
        
        if( pNIC.isEmpty()||pF_Name.isEmpty()||pL_Name.isEmpty()|| pTelephone.isEmpty()|| pAddress.isEmpty()|| pRole.isEmpty()){
            IsValid=false;
        }
      
        return IsValid;
    }
    //Login and Product Validate
    public static boolean CheckEmpty(String pNIC_pProductName,String pPassword_pSalesPrice){
        if(pNIC_pProductName.isEmpty()||pPassword_pSalesPrice.isEmpty()){
            IsValid=false;
        }
        return IsValid;
    }
    
    //Customer and ProductSupplier Validate
    public static boolean CheckEmpty(String pNIC_pBatchID,String pName_pStockPrice,String pPhone_pQuantity,String pAddress_pTotalPayment){
        if(pNIC_pBatchID.isEmpty()||pName_pStockPrice.isEmpty()||pPhone_pQuantity.isEmpty()||pAddress_pTotalPayment.isEmpty()){
 
            IsValid=false;
        }
        return IsValid;
    }
    //ProductCustomer Validate
    public static boolean CheckEmpty(String pAmount_pQuantity){  
        if(pAmount_pQuantity.isEmpty()){
            IsValid=false;
        }
        return IsValid;
    }
    
    //Supplier Validate
    public static boolean CheckEmpty(String pSupplierName,String pTelephone,String pAddress){  
        if(pSupplierName.isEmpty()||pTelephone.isEmpty()||pAddress.isEmpty()){
            IsValid=false;
        }
        return IsValid;
    }
    
    //ProductSupplier Validate
    public static boolean CheckEmpty(String pSupplier,String pProductType,String pProduct,String pBatchID,String pStockPrice,String pQuantity,String pTotalPayment){  
        if(pSupplier.isEmpty()||pProductType.isEmpty()||pProduct.isEmpty()||pBatchID.isEmpty()||pStockPrice.isEmpty()||pQuantity.isEmpty()||pTotalPayment.isEmpty()){
            IsValid=false;
        }
        return IsValid;
    }
    
    
     
    
}
