/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JForms;

import Classes.LoginClass;
import java.awt.Component;
import javax.swing.JOptionPane;

/**
 *
 * @author Kavii
 */
public class Settings_Login extends javax.swing.JFrame {

    /**
     * Creates new form Settings_Login
     */
    public Settings_Login() {
        initComponents();
        
    }
    public Settings_Login(String pEmpID,String pNIC,String pF_Name,String pL_Name,String pTelephone,String pAddress){
        initComponents();
        
        this.lblEmployeeID.setText(pEmpID);
        this.lblNIC.setText(pNIC);
        this.lblFirstName.setText(pF_Name);
        this.lblLastName.setText(pL_Name);
        this.lblPhone.setText(pTelephone);
        this.lblAddress.setText(pAddress);
        
    }
    //Valid Current Password
    public boolean isValidPassword=false;
    
    Component frame=null;
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblHome = new javax.swing.JLabel();
        lblHome1 = new javax.swing.JLabel();
        lblNIC = new javax.swing.JLabel();
        lblFirstName = new javax.swing.JLabel();
        lblLastName = new javax.swing.JLabel();
        lblPhone = new javax.swing.JLabel();
        lblAddress = new javax.swing.JLabel();
        btnUpdatePassword = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        lblHome2 = new javax.swing.JLabel();
        btnResetTable = new javax.swing.JButton();
        lblHome3 = new javax.swing.JLabel();
        lblAddress1 = new javax.swing.JLabel();
        lblEmployeeID = new javax.swing.JLabel();
        pwdc_password = new javax.swing.JPasswordField();
        pwdn_password = new javax.swing.JPasswordField();
        pwdrn_password = new javax.swing.JPasswordField();
        lblpwd = new javax.swing.JLabel();
        lblpwd1 = new javax.swing.JLabel();
        lblpwd2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(36, 47, 65));

        lblHome.setBackground(new java.awt.Color(36, 47, 65));
        lblHome.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblHome.setForeground(new java.awt.Color(255, 255, 255));
        lblHome.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHome.setText("Profile");
        lblHome.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblHome.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lblHome.setIconTextGap(235);
        lblHome.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblHome1.setBackground(new java.awt.Color(36, 47, 65));
        lblHome1.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblHome1.setForeground(new java.awt.Color(255, 255, 255));
        lblHome1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHome1.setText("Settings");
        lblHome1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblHome1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lblHome1.setIconTextGap(235);
        lblHome1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblNIC.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblNIC.setForeground(new java.awt.Color(255, 255, 255));
        lblNIC.setText("NIC");

        lblFirstName.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblFirstName.setForeground(new java.awt.Color(255, 255, 255));
        lblFirstName.setText("First Name");

        lblLastName.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblLastName.setForeground(new java.awt.Color(255, 255, 255));
        lblLastName.setText("Last Name");

        lblPhone.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblPhone.setForeground(new java.awt.Color(255, 255, 255));
        lblPhone.setText("Phone");

        lblAddress.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblAddress.setForeground(new java.awt.Color(255, 255, 255));
        lblAddress.setText("Address");

        btnUpdatePassword.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        btnUpdatePassword.setText("UPDATE");
        btnUpdatePassword.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUpdatePassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdatePasswordActionPerformed(evt);
            }
        });

        btnExit.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        btnExit.setText("EXIT");
        btnExit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblHome2.setBackground(new java.awt.Color(36, 47, 65));
        lblHome2.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblHome2.setForeground(new java.awt.Color(255, 255, 255));
        lblHome2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHome2.setText("Update Password");
        lblHome2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblHome2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lblHome2.setIconTextGap(235);
        lblHome2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btnResetTable.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        btnResetTable.setText("RESET ALL TABLES");
        btnResetTable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnResetTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetTableActionPerformed(evt);
            }
        });

        lblHome3.setBackground(new java.awt.Color(36, 47, 65));
        lblHome3.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblHome3.setForeground(new java.awt.Color(255, 255, 255));
        lblHome3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblHome3.setText("Database Settings");
        lblHome3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblHome3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lblHome3.setIconTextGap(235);
        lblHome3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        lblAddress1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        lblAddress1.setForeground(new java.awt.Color(255, 255, 255));
        lblAddress1.setText("*Please Contact System Admin to Update Your Profile Details");

        lblEmployeeID.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblEmployeeID.setForeground(new java.awt.Color(255, 255, 255));
        lblEmployeeID.setText("ID");

        pwdc_password.setBackground(new java.awt.Color(36, 47, 65));
        pwdc_password.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        pwdc_password.setForeground(new java.awt.Color(255, 255, 255));
        pwdc_password.setText("password123");
        pwdc_password.setBorder(null);
        pwdc_password.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pwdc_passwordMouseClicked(evt);
            }
        });

        pwdn_password.setBackground(new java.awt.Color(36, 47, 65));
        pwdn_password.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        pwdn_password.setForeground(new java.awt.Color(255, 255, 255));
        pwdn_password.setText("password123");
        pwdn_password.setBorder(null);
        pwdn_password.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pwdn_passwordMouseClicked(evt);
            }
        });

        pwdrn_password.setBackground(new java.awt.Color(36, 47, 65));
        pwdrn_password.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        pwdrn_password.setForeground(new java.awt.Color(255, 255, 255));
        pwdrn_password.setText("password123");
        pwdrn_password.setBorder(null);
        pwdrn_password.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pwdrn_passwordMouseClicked(evt);
            }
        });

        lblpwd.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblpwd.setForeground(new java.awt.Color(255, 255, 255));
        lblpwd.setText("Current Password");

        lblpwd1.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblpwd1.setForeground(new java.awt.Color(255, 255, 255));
        lblpwd1.setText("New Password");

        lblpwd2.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        lblpwd2.setForeground(new java.awt.Color(255, 255, 255));
        lblpwd2.setText("Retype New Password");

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("NIC :");

        jLabel2.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("First Name :");

        jLabel3.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Last Name : ");

        jLabel4.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Phone :");

        jLabel5.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Address :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblHome2, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnUpdatePassword, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblAddress1, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lblpwd2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblpwd1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblpwd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(89, 89, 89)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pwdc_password, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                                    .addComponent(pwdn_password)
                                    .addComponent(pwdrn_password)
                                    .addComponent(jSeparator1)
                                    .addComponent(jSeparator2)
                                    .addComponent(jSeparator3)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblHome, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(46, 46, 46)
                                .addComponent(lblEmployeeID, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lblNIC, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                        .addComponent(lblPhone, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                        .addComponent(lblHome1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(109, 109, 109)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(129, 129, 129)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblLastName, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(lblHome3, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(81, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnResetTable, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(lblHome1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblEmployeeID, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(lblHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNIC, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLastName, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addComponent(lblHome2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pwdc_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblpwd))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblpwd1)
                    .addComponent(pwdn_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblpwd2)
                    .addComponent(pwdrn_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(btnUpdatePassword, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(lblHome3)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnResetTable, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAddress1)
                .addContainerGap(219, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdatePasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdatePasswordActionPerformed
        // TODO add your handling code here: 
        String NIC=this.lblNIC.getText();
        String CurrentPassword=String.valueOf(pwdc_password.getPassword());
        String NewPassword=String.valueOf(pwdn_password.getPassword());
        String RNewPassword=String.valueOf(pwdrn_password.getPassword());
        LoginClass checkcurrentlogin=new LoginClass();
        isValidPassword=checkcurrentlogin.getLogin(NIC, CurrentPassword);
        if(isValidPassword==true){
            if(NewPassword.equals(RNewPassword)){
                
                int SelectedOption=JOptionPane.showConfirmDialog(frame,"Do You Want To Update?");
                if(SelectedOption==0){
                    LoginClass updatelogin=new LoginClass();
                    updatelogin.UpdatePassword(NIC,NewPassword);
                    JOptionPane.showMessageDialog(frame,"Password Updated Sucessfully!");
                }
            }
            else{
                JOptionPane.showMessageDialog(frame,"New Password and Retyped Password Mismatch!","Warning",JOptionPane.WARNING_MESSAGE);
            }
        }
        else{
            JOptionPane.showMessageDialog(frame,"Invalid Current Password!","Warning",JOptionPane.WARNING_MESSAGE);
        }
        
        
    }//GEN-LAST:event_btnUpdatePasswordActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnResetTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetTableActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnResetTableActionPerformed

    private void pwdc_passwordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pwdc_passwordMouseClicked
        // TODO add your handling code here:
        pwdc_password.setText("");
    }//GEN-LAST:event_pwdc_passwordMouseClicked

    private void pwdn_passwordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pwdn_passwordMouseClicked
        // TODO add your handling code here:
        pwdn_password.setText("");
    }//GEN-LAST:event_pwdn_passwordMouseClicked

    private void pwdrn_passwordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pwdrn_passwordMouseClicked
        // TODO add your handling code here:
        pwdrn_password.setText("");
    }//GEN-LAST:event_pwdrn_passwordMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Settings_Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Settings_Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Settings_Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Settings_Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Settings_Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnResetTable;
    private javax.swing.JButton btnUpdatePassword;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblAddress1;
    private javax.swing.JLabel lblEmployeeID;
    private javax.swing.JLabel lblFirstName;
    private javax.swing.JLabel lblHome;
    private javax.swing.JLabel lblHome1;
    private javax.swing.JLabel lblHome2;
    private javax.swing.JLabel lblHome3;
    private javax.swing.JLabel lblLastName;
    private javax.swing.JLabel lblNIC;
    private javax.swing.JLabel lblPhone;
    private javax.swing.JLabel lblpwd;
    private javax.swing.JLabel lblpwd1;
    private javax.swing.JLabel lblpwd2;
    private javax.swing.JPasswordField pwdc_password;
    private javax.swing.JPasswordField pwdn_password;
    private javax.swing.JPasswordField pwdrn_password;
    // End of variables declaration//GEN-END:variables
}
